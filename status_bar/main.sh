#Colors
GREEN="\033[32m"
BLUE="\033[34m"
MAGENTA="\033[35m"
CYAN="\033[36m"

SEPARATOR="\033[0m | "

while : ; do clear
RAMTOTAL="`./mem.sh MemTotal:`B"
RAMAVAIL="`./mem.sh MemAvailable:`B"

printf "$BLUE$USER"

printf "$SEPARATOR$CYAN"
echo -n `date +%H:%M`

printf "$SEPARATOR$GREEN"
printf "RAM: $RAMTOTAL"

printf "$SEPARATOR$MAGENTA"
printf "RAM availible: $RAMAVAIL"

sleep 10
done
